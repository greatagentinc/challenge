# Great Agent Coding Challenge

## Deadline

Task should be done in 72 hours from the date and time you start the task. 
This task is scoped to take no more than 6 hours assuming previous experience with Laravel and PHPUnit. 

## Requirements

- Task should be implemented with Laravel Framework with a MySQL database.

- The code you write needs to follow the PSR-2 coding standard and the PSR-4 autoloading standard.

- Make your project installation in one command (for example `composer install`).  
It should include all routines such application key generation, migration, assets publishing and so on. 
Please double check installation from scratch. 

# Tasks objects

As globalization is increasing, we want you to create site that allows the user to call you using a local number 
in their country no matter where they are. You should build this App using the Twilio API. 
Create a with the following functionality:

- The visitor should be prompted with 3 countries’ flags (e.g. USA, Ireland and Denmark) 
where Twilio offers local numbers.
- The visitor should choose one country by clicking flag.
- Find an available number in that country, buy the number and show the newly acquired phone number to the visitor 
(keep in mind test account allow only one number to buy).
- Write one unit test using PHPUnit for the most critical function of your application 
and cover it with a well-written unit test.
- also we have [bonus task](docs/bonus.md)

# How to Send and Discuss Your Result

- Skype: jasper.juhl (also connect to Jasper if you have questions)
- Make a pull request from your repo when you are done. This will allow us to see the changes you have made as well as making a code review directly in bitbucket.

# WORKFLOW

1. Clone that repository and make your own **private** one.
2. Create new branch (for example challenge) and push your challenge code.
3. Create pull request and send it to us.
4. Provide access to your repository to bitbucket users:
- @terrasoff
- @greatagent

# INSTRUCTIONS

## INSTALL

You need provide [documentation](docs/install.md) how to install your app

## TWILIO 

You need get a free test API key from [Twilio](www.twilio.com). 
See [quick-start](https://www.twilio.com/docs/quickstart/php/twiml)

## DOCKER 

You may use our [docker configuration](docs/docker.md) to deploy environment fast.