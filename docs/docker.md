# Docker environment

Setup [docker](https://docs.docker.com/engine/installation/)
and [docker compose](https://docs.docker.com/compose/install/) to your system

Add record to your system hosts file to make challenge host resolving

`127.0.0.1 challenge.greatagent.dev`

Execute `docker-composer up` to run docker environment for project

## Docker commands

All commands on server running through php7 container. And we have helper to execute php-container commands

### Composer commands

Composer running that way:

`./docker/commands/exec "composer install"`

### Artisan commands

To run artisan commands execute

`./docker/commands/exec "php artisan"`